﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Northwind
{
    public partial class Form2 : Form
    {
        int _productid;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Product product)
        {
            _productid = product.ProductID;
            InitializeComponent();
            textBox1.Text = product.ProductName;
            numericUpDown1.Value = (decimal)product.UnitPrice;
            numericUpDown2.Value = product.UnitsInStock;
            numericUpDown3.Value = product.UnitsOnOrder;


        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            Product product = GetProductInFor();
            string path = "Data Source=thongnguyen;Initial Catalog=Northwind;Integrated Security=True";
            // NHỚ THÊM THƯ VIỆN VÀO
            SqlConnection conn = new SqlConnection(path);
            // CÂU TRUY VẤN
            string sql = string.Format("Update Products set ProductName='{0}',UnitsOnOrder={1},UnitPrice={2},UnitsInStock={3} WHERE ProductID={4}",
                product.ProductName,product.UnitsOnOrder, product.UnitPrice, product.UnitsInStock,_productid);
            // MỞ KẾT NỐI
            SqlCommand sqlcommand = new SqlCommand(sql, conn);
            conn.Open();
            int rs = sqlcommand.ExecuteNonQuery();
            if (rs > 0)
            {
                MessageBox.Show("Update Products Successfully!!!!!");
               
            }
            conn.Close();

        }

        private Product GetProductInFor()
        {
            var product = new Product();
            product.ProductName = textBox1.Text;
            product.UnitPrice = (float)numericUpDown1.Value;
            product.UnitsInStock = (int)numericUpDown2.Value;
            product.UnitsOnOrder = (int)numericUpDown3.Value;
            return product;

        }

        
    }
}
