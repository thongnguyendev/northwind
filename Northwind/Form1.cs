﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Northwind
{
    public partial class FormProduct : Form
    {
        public FormProduct()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string path = "Data Source=thongnguyen;Initial Catalog=Northwind;Integrated Security=True";
            // NHỚ THÊM THƯ VIỆN VÀO
            SqlConnection conn = new SqlConnection(path);
            // CÂU TRUY VẤN
            string sql = "select * from Products";
            //BỘ CHUYỂN ĐỔI DỮ LIỆU
            SqlDataAdapter sqldata = new SqlDataAdapter(sql, conn);
            //BẢNG CHỨA DỮ LIỆU
            DataTable datatable = new DataTable();
            sqldata.Fill(datatable);
            GrvData.DataSource = datatable;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int ProductID = int.Parse(GrvData.CurrentRow.Cells["ProductID"].Value.ToString());
            string path = "Data Source=thongnguyen;Initial Catalog=Northwind;Integrated Security=True";
            // NHỚ THÊM THƯ VIỆN VÀO
            SqlConnection conn = new SqlConnection(path);
            // CÂU TRUY VẤN
            string sql = string.Format("Delete from  [Order Details] where ProductID={0}", ProductID);
            // MỞ KẾT NỐI
            SqlCommand sqlcommand = new SqlCommand(sql, conn);
            conn.Open();
            int rs = sqlcommand.ExecuteNonQuery();
            if (rs > 0)
            {
                MessageBox.Show("Delete Product Successfully!!!!!");
                GrvData.Rows.Remove(GrvData.CurrentRow);
            }
            // ĐÓNG KẾT NỐI
            conn.Close();
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Product product = new Product();
            {
                int ProductID = int.Parse(GrvData.CurrentRow.Cells["ProductID"].Value.ToString());
                string ProductName = GrvData.CurrentRow.Cells["ProductName"].Value.ToString();
                int SupplierID = int.Parse(GrvData.CurrentRow.Cells["SupplierID"].Value.ToString());
                int CategoryID = int.Parse(GrvData.CurrentRow.Cells["CategoryID"].Value.ToString());
                string QuantityPerUnit = GrvData.CurrentRow.Cells["QuantityPerUnit"].Value.ToString();
                float UnitPrice = float.Parse(GrvData.CurrentRow.Cells["UnitPrice"].Value.ToString());
                int UnitsInStock = int.Parse(GrvData.CurrentRow.Cells["UnitsInStock"].Value.ToString());
                int UnitsOnOrder = int.Parse(GrvData.CurrentRow.Cells["UnitsOnOrder"].Value.ToString());
                int ReorderLevel = int.Parse(GrvData.CurrentRow.Cells["ReorderLevel"].Value.ToString());
                bool Discontinued = bool.Parse(GrvData.CurrentRow.Cells["Discontinued"].Value.ToString());
            };
            Form2 frm = new Form2(product);
            frm.ShowDialog();

        }

        private void inSertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Done Insert Information Products !!!!Can You refesh Data!");
        }

        public int ProductID { get; set; }

        private void btn_add_Click(object sender, EventArgs e)
        {
            //string path = "Data Source=thongnguyen;Initial Catalog=Northwind;Integrated Security=True";
            // NHỚ THÊM THƯ VIỆN VÀO
            //SqlConnection conn = new SqlConnection(path);
            //string sql = string.Format("Insert into Products values('{0}','{1}',{2},{3},{4},{5},{6})", "Iphone11",  "Iphone chinh hang", 29990, 12, 13, 5, 2, 1);
            // MỞ KẾT NỐI
           // conn.Open();
            //SqlCommand sqlcommand = new SqlCommand(sql, conn);
            //var rs = sqlcommand.ExecuteNonQuery();
            //MessageBox.Show("Total User: " + rs.ToString() + " dữ liệu");
            MessageBox.Show("Done Insert Information Products !!!!Can You refesh Data!");

            // ĐÓNG KẾT NỐI
           // conn.Close();
        }
    }
}
